To compile enter the command "gcc -o main main.c -lm" , then "./main"

Bitbucket link: https://bitbucket.org/EmmettL96/os_ca1_emmett_lynch/commits/all

Note: I'm not sure why the physical_memory text file is displaying the
page table entries horizontally in windows. In Ubuntu the page table entries in
the text file are displaying normally.
