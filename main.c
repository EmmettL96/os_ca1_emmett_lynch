#include <stdio.h>
#include <stdlib.h>
#include "functions/functions.c"

struct table
{
	unsigned short pageNum[36];
	unsigned short frameNum[36];
	char content[36];
	unsigned short physicalPageNum[36];
};

struct table getPageTableEntries();

int main()
{

	int rand = getRandomNum();

	char *process = (char *)malloc(rand);

	printf("Alocated %d of bytes for process.\n", rand);

	if(sizeof(*process) == NULL)
	{
		printf("Not enough memory.");
	}

	else
	{

		int page_size = 256;

		//getting the number of bits required for the offset
		int pageOffset = get_page_offset(page_size);

		//alocating 512 bytes for page table
		struct table *pte = (char *)malloc(512);

		//populating and translating page table entries
		*pte = getPageTableEntries(pageOffset);

		//writing to file
		printTable(*pte);

		//allows the user to search the page table
		searchPageTable(*pte);

		return 0;
	}
}

struct table getPageTableEntries(int offset)
{
	srand(time(NULL));
	int randomNum;

	struct table pte;

  unsigned short vpn      = 0x54F3;
  unsigned short frame  = 0x21;

	unsigned short address = 0;

	//translating each page number
	for(int i = 0; i < 36; i++)
	{
		randomNum = rand() % 7;

		pte.pageNum[i] = vpn + i;
		pte.frameNum[i] = frame + i;

		address = translateAddress(pte.pageNum[i], pte.frameNum[i], offset);

		pte.physicalPageNum[i] = address;

		pte.content[i] = randomizeContent(randomNum);
	}

	return pte;
}

void printTable(struct table pte)
{
	//writing to file
	FILE *fp;

	fp = fopen("physical_memory.txt", "w+");

	fprintf(fp,"VPN\t\tFrame\t\tPPN\t\tContent\n");

	for(int i = 0; i < 36; i++)
	{
		fprintf(fp,"0x%X\t\t%X\t\t0x%X\t\t%c\n", pte.pageNum[i], pte.frameNum[i], pte.physicalPageNum[i], pte.content[i]);
	}

	fclose(fp);
}

void searchPageTable(struct table pte)
{
	//searching the page array and using the index matching the query
	//to print out the columns of that index
	unsigned short query;

	printf("Enter a value in hex to search for a page: \n");

	scanf("%X", &query);

	printf("You have entered %X (HEX) %d(DEC)\n", query, query);

	for(int i = 0; i < 20; i++)
	{
		if(query == pte.pageNum[i])
		{
			printf("Page number: 0x%X\tFrame: %X\tPhysical Page Number: 0x%X\tContent%c\n", pte.pageNum[i], pte.frameNum[i], pte.physicalPageNum[i], pte.content[i]);

			break;
		}

		else if(i == 19)
		{
			printf("No page matched your query.\n");
		}

	}

	printf("End of search\n");
}
