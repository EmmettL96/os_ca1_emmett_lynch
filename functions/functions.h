#ifndef	FUNCTIONS
#define	FUNCTIONS

int get_page_offset(int offset);
void allocate_mem(int arr, int g);
int getRandomNum();
int translateAddress(unsigned int page, unsigned int frame);
char randomizeContent(int r);

#endif
