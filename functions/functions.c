#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

int get_page_offset(int offset)
{
	int i = 0;

	//page size is equal to 256 bytes
	//log2 of 256 is equal to 8
	//Which is the number of bits needed for the page offset
	i = log10(offset)/log10(2);

	//printf("Page Offset is: %d \n", i);

	return i;
}

/*
void allocate_mem(char* arr, int num)
{
	int byteSize = getRandomNum();

	arr = (char*)calloc(byteSize, 2048);

	printf("Memory has been allocated\n");

	if(arr == NULL)
	{
		printf("No memory available.");
	}

	else
	{
		for(int i = 0; i < 10; i++)
		{
			arr[i] = "a";
			printf("The size of the element is: %d\n", sizeof(arr[i]));
		}

	}
}
*/

int getRandomNum()
{
	//generates a random between 2 ranges
	int randomNum;

	int minNum = 2048;
	int maxNum = 20480;


	srand(time(NULL));
	randomNum = rand() % (maxNum + 1 - minNum) + minNum;

	return randomNum;
}

int translateAddress(unsigned int pageNum, unsigned int frame, unsigned int num_of_offset_bits)
{

	//the bitmask discards the first 8 bits
	unsigned short offsetMask = 0x00FF;

	unsigned short offset = pageNum & offsetMask;

	//Left shift 8 bits
	unsigned short pAddress = frame << num_of_offset_bits;

	//appending offset to physical address
	pAddress |= offset;

	return pAddress;
}

char randomizeContent(int r)
{
	char letters[20] = {'Z', 'Y', 'X', 'G', 'T', 'Q', 'V', 'K'};

	char content = letters[r];

	return content;
}

